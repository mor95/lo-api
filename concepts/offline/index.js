module.exports = {
    getLOConcepts: require('./getLOConcepts.js'),
    addLOConcepts: require('./addLOConcepts.js'),
    deleteLOConcepts: require('./deleteLOConcepts.js')
}