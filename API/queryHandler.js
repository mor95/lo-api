const _ = require('lodash')
const APILocationSources = require('./APILocationSources.js')
const props = require('./queryHandlerProps.js')
const qs = require('qs')

module.exports = function (args) {
    if (!_.isObject(args))
        throw new Error('Invalid arguments. An object is expected.')

    var self = this
    self.APILocationSources = new APILocationSources(args)
    self.props = new props(args)

    self.request = function(_args){
        if(!_.isObject(_args.arguments))
            _args.arguments = {}
            
        if(_args.returnQuery === true) return {
            arguments: _args.arguments,
            method: _args.method
        }
    
        _args.defaultRequestType = 'GET'
    
        if(_.isString(_args.requestType))
            _args.requestType = _args.requestType
        else
            _args.requestType = _args.defaultRequestType

        if(_args.requestType === 'GET')
            return fetch(self.APILocationSources.location + '?' + qs.stringify(_args), {
                method: _args.requestType
            })
        
        return fetch(self.APILocationSources.location, {
            method: _args.requestType,
            body: JSON.stringify({
                method: _args.method,
                arguments: _args.arguments
            })
        })
    }
}