const _ = require('lodash')
const moodleCookie = require('./moodleCookie.js')
const fetch = require('whatwg-fetch')
const Cookies = require('js-cookie')

module.exports = function(args){
    if (!_.isObject(args))
        throw new Error('Invalid arguments. An object is expected.')
    
    if(!_.isString(args.remoteURL))
        throw new Error('Invalid remoteURL. A string is expected.')

    const self = this
    
    self.response = {
        defaultLocation: './_ws/API.php',
        isLocalhost: moodleCookie.isLocalhost(),
        remoteURL: args.remoteURL
    }
    
    if(Cookies.get('RedirectToRemoteURL')){
        self.response.APIFolder = self.response.remoteURL + '/_ws/API.php'
    }
    else{
        if(location.pathname.indexOf('extension/') > -1){
            self.response.APIFolder = '/extension/_ws/API.php'
        }
        else{
            self.response.APIFolder = '/_ws/API.php'
        }
    }
    
    self.response.remoteLocation = self.response.remoteURL + self.response.APIFolder
    
    if (_.isString(args.location)) {
        if (args.showFeedback !== false) {
            console.warn('Using custom API Location ' + args.location)
        }
        self.response.location = args.location
    } else {
        if (self.response.isLocalhost) {
            if (args.showFeedback !== false) {
                console.warn('Retrieving response from remote API from localhost environment. Enabling CORS in your browser might be necessary.')
            }
            self.response.location = self.response.remoteLocation
        } else {
            self.response.location = self.response.APIFolder
        }
    }
    
    return self.response
}