module.exports = {
    getLOActivities: require('./getLOActivities.js'),
    addLOActivities: require('./addLOActivities.js'),
    deleteLOActivities: require('./deleteLOActivities.js')
}