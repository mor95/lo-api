const _ = require('lodash')
const courseHandler = require('./courseHandler.js')
const checkTypes = require('check-types')

module.exports = function (args) {
    checkTypes.assert.object(args)
    return courseHandler(_.merge({
        method: 'getCourseActivities'
    }, args))
}